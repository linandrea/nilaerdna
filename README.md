- 👋 Hi, I’m Andrea.
- 🧑🏻‍💻 I'm currently working as a developer.
- 💻 I usually work with TypeScript.
- 🌱 Some hobbies of mine are:
  - ⌨️ Custom Keyboards
  - 🖱 Modded Mices
  - 💾 Homelabs
  - 🕹 Videogames
  - 🎧 Music
<!-- - 👀 I’m learning C++ on my own. -->
<!-- - 📫 How to reach me me@andrealin.it -->